#include <iostream>

class MyClass {
    short a; // 2 bytes + 2 bytes offset
    int b; // 4 bytes
    short c; // 2 bytes + 2 bytes offset
};

class MyClass2 {
    int b; // 4 bytes
    short a; // 2 bytes
    short c; // 2 bytes
};

int main() {
    std::cout << "sizeof(MyClass): " << sizeof(MyClass) << std::endl;

    std::cout << "sizeof(MyClass2): " << sizeof(MyClass2) << std::endl;

    return 0;
}

