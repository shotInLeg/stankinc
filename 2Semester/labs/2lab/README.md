# Лабораторная работа №2

## Сторонние библиотеки C++ на примре libcurl (3rd-party libraries, libcurl example)

### Установка из исходников (Install from sources)

Загружаем исходный код (Download source code)
```bash
wget https://curl.haxx.se/download/curl-7.64.0.tar.gz -O curl-7.64.0.tar.gz
```

Распаковываем тарбол (архив) (Extract tarball (archive))
```bash
tar -xvf curl-7.64.0.tar.gz
```

Заходим в распакованный архив (Change dir)
```bash
cd curl-7.64.0
```

Конфигурируем сборку (Configure build)
```bash
./configure --prefix=/usr/local
```

Собираем (Build)
```bash
make
```

Устанавливаем (Install)
```bash
sudo make install
```

Установка 32-bit версии (Install 32-bit version)
```bash
sudo apt-get install libcurl3:i386
```

Загружаем исходный код (Download source code)
```bash
wget https://github.com/jpbarrette/curlpp/archive/v0.8.1.tar.gz -O curlpp-v0.8.1.tar.gz
```

Распаковываем тарбол (архив) (Extract tarball (archive))
```bash
tar -xvf curlpp-v0.8.1.tar.gz
```

Заходим в распакованный архив (Change dir)
```bash
cd curlpp-v0.8.1
```

Создаем папку для сборки (Create build folder)
```bash
mkdir build
```

Заходим в папку для собрки (Change dir)
```bash
cd build
```

Конфигурируем сборку CMake (Configure CMake build)
```bash
cmake ..
```

Собираем (Build)
```bash
make
```

Устанавливаем (Install)
```bash
sudo make install
```

Получение флагов компиляции (Get compile flags)
```bash
curlpp-config --cflags --libs
```

### Проверка (Test)

Создать файл l2e1.cpp и записать в него следующий код (Create file l2e1.cpp and write with code)
```cpp
#include <curlpp/cURLpp.hpp>
#include <curlpp/Easy.hpp>
#include <curlpp/Options.hpp>


using namespace curlpp::options;

int main(int, char **)
{
    try
    {
        // That's all that is needed to do cleanup of used resources (RAII style).
        curlpp::Cleanup myCleanup;

        // Our request to be sent.
        curlpp::Easy myRequest;

        // Set the URL.
        myRequest.setOpt<Url>("http://httpbin.org/ip");

        // Send request and get a result.
        // By default the result goes to standard output.
        myRequest.perform();
    }

    catch(curlpp::RuntimeError & e)
    {
        std::cout << e.what() << std::endl;
    }

    catch(curlpp::LogicError & e)
    {
        std::cout << e.what() << std::endl;
    }

  return 0;
}
```

Скомпилировать (Complie)
```bash
g++ l2e1.cpp -o l2e1 -std=c++11 -I/usr/local/include -L/usr/local/lib -lcurlpp -lcurl
```

Запустить (Excecute)
```bash
./l2e1
```

Вывод (Output)
```json
{
  "origin": "XXX.180.XXX.112, XXX.180.XXX.112"
}
```

