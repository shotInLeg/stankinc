#include <iostream>
#include <map>


template<class T>
class SharedPtr {
	T* data;
	static std::map<T*, int> count;

public:
	SharedPtr() {
		data = new T();
        std::cerr << "Allocate pointer " << data << std::endl;
	}

	SharedPtr(T* data) {
        std::cerr << "Catch pointer " << data << std::endl;
		this->data = data;
		this->count[this->data]++;
	}

    SharedPtr(const SharedPtr<T>& b) {
        std::cerr << "Copy pointer " << b.data << std::endl;
        this->data = b.data;
        this->count[this->data]++;
    }

	T& operator*() {
		return *(this->data);
	}

    ~SharedPtr() {
        if (count[this->data] == 1) {
            std::cerr << "Delete data " << this->data << std::endl;
            delete this->data;
        }
        count[this->data]--;
    }
};

template<class T>
std::map<T*, int> SharedPtr<T>::count{};


int main() {
	SharedPtr<int> a(new int(5));
    SharedPtr<int> b(a);

	std::cout << *a << std::endl;
    std::cout << *b << std::endl;

    *a = 7;

    std::cout << *a << std::endl;
    std::cout << *b << std::endl;

	return 0;
}

