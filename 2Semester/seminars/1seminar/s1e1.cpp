#include <iostream>


/*
 * Class - "template" of object
 * Class - type of object
 */
class Circle {
private:
    /*
     * Class fields - information describing each class object (instances of a class)
     * Поля класса - информация описывающая каждый объект класса (экземпляр класса)
     */
    double radius;

public:
    /*
     * Constructor - a special method of a class to create objects (instances of a class)
     * Конструктор класса - специальный функция (метод) класса для создания объектов
     *  класса (экземпляров класса)
     *
     * Default constructor - constructor without args to fill class fields with default values
     * Конструктор по-умолчанию - конструктор без параметро для заполнения полей класса
     *  значениями по-умолчани
     */
    Circle(): radius(0) {}  // NameOfClass()[: initialization list] {}
    /* Without initialization list
     * Тоже самое без списка инициалтзации
    Circle() {
        radius = radius;
    }
     */

    /*
     * Constuctor with args - constructor with args to fill class fields passed values
     * Конструктор с параметрами - конструктор с параметрами для заполенения полей класса
     *  переданными значениями
     */
    Circle(double radius): radius(radius) {}  // NameOfClass(args)[: initialization list] {}
    /* Without initialization list
     * Тоже самое без списка инициалтзации
    Circle(double radius): {
        this->radius = radius;
    }
     */

    /*
     * Copy Constructor - constructor to cretae new class object (instances of a class) through copying
     *  another object of this class
     * Конструктор копирования - конструктор для создания объекта класса (экземпляра класса) через копирование
     *  другого объекта этого класса
     */
    Circle(const Circle& obj): radius(obj.radius) {}  // NameOfClass(const NameOfClass& var)[: initialization list] {}
    /* Without initialization list
     * Тоже самое без списка инициалтзации
    Circle(const Circle& obj) {
        this->radius = obj.radius;
    }
     */

    /*
     * Method - function inside class
     * Метод - функция принадлежащая классу
     *
     * (const - method does not change fileds of class object)
     * (const - значит что метод не изменяет поля объекта класса)
     */
    double area() const {  // return type value nameOfMethod([args]) [modifiers] {}
        return radius * radius * 3.14;
    }

    /*
     * Method getter - method return value or field or property of class object (instance of a class)
     * Метод геттер - метод возвращающий значение поля или свойсва объекта класса (экземпляра класса)
     */
    double getRadius() const {
        return radius;
    }

    /*
     * Method setter - method sets value of field or property of class object (instance of a class)
     * Метод сеттер - метод устанавливающий значение поля или свойства объекта класса (экземпляра класса)
     */
    void setRadius(double radius) {
        if (radius < 0 || radius > 100000) {
            std::cerr << "Invalid value " << radius << " of argument 'radius' in Circle::setRadius" << std::endl;
            return;
        }
        this->radius = radius;
    }
};


int main() {
    std::cout << "Seminar 1, Example 1" << std::endl << std::endl;


    Circle circle1;
    std::cout << "circle1.getRadius(): " << circle1.getRadius() << std::endl;
    std::cout << "circle1.setRadius(-10)" << std::endl;
    circle1.setRadius(-10);
    std::cout << "circle1.getRadius(): " << circle1.getRadius() << std::endl;
    std::cout << "circle1.area(): " << circle1.area() << std::endl;
    std::cout << std::endl;

    Circle circle2(5);
    std::cout << "circle2.area(): " << circle2.area() << std::endl;
    std::cout << std::endl;

    Circle circle3(circle2);
    std::cout << "circle3.area(): " << circle3.area() << std::endl;
    std::cout << std::endl;

    return 0;
}

