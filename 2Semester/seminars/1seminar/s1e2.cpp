#include <iostream>
#include <string>

/*
 * Base class
 */
class Animal {
private:
    std::string name;

public:
    Animal(): name("unknow") {}
    Animal(const std::string& name): name(name) {}

    /*
     * Common method - a method whose implementation is common to all child classes
     * Общий метод - метод, реализация которого общая для всех классов
     */
    std::string getName() const {
        return this->name;
    }

    void voice() const {
        std::cout << "I have not voice" << std::endl;
    }
};


/*
 * Child class
 */
class Dog : public Animal {  // class NameOfClass : inheritance type NameOfBaseClass
public:
    Dog() {}
    Dog(const std::string& name): Animal(name) {}  // NameOfClass(args): NameOfBaseClass(args), [initialization list] {}

    /*
     * Overrided method - method with the same name and parameters as in the base class,
     *  but with a different implementation
     * Переопределенный метод - метод с таким же именем и параметрами как в базовом классе,
     *  но с другой реализацией
     */
    void voice() const {
        std::cout << "Gav-Gav-Gav" << std::endl;
    }
};


/*
 * Child class
 */
class Cat : public Animal {
public:
    Cat() {}
    Cat(const std::string& name): Animal(name) {}

    void voice() const {
        std::cout << "Myau-Myau-Mayu" << std::endl;
    }
};


int main() {
    std::cout << "Seminar 1, Example 2" << std::endl << std::endl;

    Dog dog("Rex");
    std::cout << dog.getName() << std::endl;
    dog.voice();
    std::cout << std::endl;

    Cat cat;
    std::cout << cat.getName() << std::endl;
    cat.voice();

    return 0;
}

