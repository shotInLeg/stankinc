#include <iostream>
#include <string>


/*
 * Abstract base class - class with 1 or more pure virtual function
 * Абстрактный базоый класс -  класс с 1 и более чисто виртуальной функцией
 */
class Animal {
/*
 * protected - like private but can access from child classes
 * protected - как private, но есть доступ из дочернх классов
 */
protected:  // like private but can access from child classes
    std::string name;

public:
    Animal(): name("unknow") {}
    Animal(const std::string& name): name(name) {}

    /*
     * Virtual destructor - if class have 1 or more virtual methods need implemant viratl destructor
     * Виртуальный деструктор - если класс имеет 1 и более виртуальных методов нужно реализовать 
     *  виртуальный декструктор
     */
    virtual ~Animal() {}

    std::string getName() const {
        return this->name;
    }

    /*
     * Pure virtual function - virtual method without implemant
     * Чисто виртуальная функция - виртуальный метод без реализации
     */
    virtual void voice() const = 0;
};


class Dog : public Animal {
public:
    Dog() {}
    Dog(const std::string& name): Animal(name) {}

    /*
     * override - method override virtaul method from base class
     * override - метод переопределяет виртуальный метод из базового класса
     */
    virtual void voice() const override {
        std::cout << this->name << ": " << "Gav-Gav-Gav" << std::endl;
    }
};


class Cat : public Animal {
public:
    Cat() {}
    Cat(const std::string& name): Animal(name) {}

    virtual void voice() const override {
        std::cout << this->name << ": " << "Myau-Myau-Mayu" << std::endl;
    }
};


int main() {
    std::cout << "Seminar 3, Example 1" << std::endl << std::endl;

    Animal * zoo[] = {
        new Dog("dog1"),
        new Cat("cat1"),
        new Dog("dog2"),
        new Cat("cat2")
    };

    for(Animal * animal : zoo) {
        animal->voice();
    }

    for(Animal * animal : zoo) {
        delete animal;
    }

    return 0;
}

