#include <iostream>
#include <string>


class Animal {
protected:
    std::string name;

public:
    Animal(): name("unknow") {
        std::cout << "Default constructor of Animal" << std::endl;
    }
    Animal(const std::string& name): name(name) {
        std::cout << "Constructor with args of Animal" << std::endl;
    }
    ~Animal() {
        std::cout << "Destructor of Animal" << std::endl;
    }

    virtual void voice() const = 0;
};


class Dog : public Animal {
public:
    Dog() {
        std::cout << "Default constructor of Dog" << std::endl;
    }
    Dog(const std::string& name): Animal(name) {
        std::cout << "Constructor with args of Dog" << std::endl;
    }
    ~Dog() {
        std::cout << "Destructor of Dog" << std::endl;
    }

    virtual void voice() const override {
        std::cout << this->name << ": " << "Gav-Gav-Gav" << std::endl;
    }
};


class Cat : public Animal {
public:
    Cat() {
        std::cout << "Default constructor of Cat" << std::endl;
    }
    Cat(const std::string& name) {
        this->name = name;
        std::cout << "Constructor with args of Cat" << std::endl;
    }
    ~Cat() {
        std::cout << "Destructor of Cat" << std::endl;
    }

    virtual void voice() const override {
        std::cout << this->name << ": " << "Myau-Myau-Mayu" << std::endl;
    }
};


int main() {
    std::cout << "Seminar 3, Example 4" << std::endl << std::endl;

    std::cout << "Section 1" << std::endl << std::endl;
    if (true) {
        Dog dog1("dog1");
        Cat cat1("cat1");
    }
    std::cout << std::endl;

    std::cout << "Section 2" << std::endl << std::endl;
    if (true) {
        Animal * dog2 = new Dog("dog2");
        Animal * cat2 = new Cat("cat2");

        delete dog2;
        delete cat2;
    }
    std::cout << std::endl;

    return 0;
}

