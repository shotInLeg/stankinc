#include <iostream>
#include <string>


class Animal {
protected:  // like private but can access from child classes
    std::string name;

public:
    Animal(): name("unknow") {}
    Animal(const std::string& name): name(name) {}
    ~Animal() {}

    std::string getName() const {
        return this->name;
    }

    void voice() const {
        std::cout << "I have not voice" << std::endl;
    }
};


class Dog : public Animal {
public:
    Dog() {}
    Dog(const std::string& name): Animal(name) {}

    void voice() const {
        std::cout << this->name << ": " << "Gav-Gav-Gav" << std::endl;
    }
};


class Cat : public Animal {
public:
    Cat() {}
    Cat(const std::string& name): Animal(name) {}

    void voice() const {
        std::cout << this->name << ": " << "Myau-Myau-Mayu" << std::endl;
    }
};


int main() {
    std::cout << "Seminar 3, Example 2" << std::endl << std::endl;

    Animal * zoo[] = {
        new Dog("dog1"),
        new Cat("cat1"),
        new Dog("dog2"),
        new Cat("cat2")
    };

    for(Animal * animal : zoo) {
        animal->voice();
    }

    for(Animal * animal : zoo) {
        delete animal;
    }

    return 0;
}

