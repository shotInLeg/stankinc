#include <iostream>


class Point {
private:
    int x;
    int y;
public:
    Point(int x, int y): x(x), y(y) {}

    int getX() const {
        return x;
    }
    int getY() const {
        return y;
    }

    void setX(int xx) {
        x = xx;
    }
    void setY(int yy) {
        y = yy;
    }
};


class HackPoint {
public:  // public
    int x;
    int y;
public:
    HackPoint(int x, int y): x(x), y(y) {}

    int getX() const {
        return x;
    }
    int getY() const {
        return y;
    }

    void setX(int xx) {
        x = xx;
    }
    void setY(int yy) {
        y = yy;
    }
};


int main() {
    std::cout << "Seminar 3, Example 5" << std::endl << std::endl;

    Point * point = new Point(1, 2);

    std::cout << "point->getX(): " << point->getX() << std::endl;
    std::cout << "point->getY(): " << point->getY() << std::endl;
    std::cout << std::endl;

    HackPoint * hackPoint = reinterpret_cast<HackPoint*>(point);
    hackPoint->x = 3;
    hackPoint->y = 4;

    std::cout << "point->getX(): " << point->getX() << std::endl;
    std::cout << "point->getY(): " << point->getY() << std::endl;
    std::cout << std::endl;

    std::cout << "point: " << point << "; &point: " << &point << std::endl;
    std::cout << "hackPoint: " << hackPoint << "; &hackPoint: " << &hackPoint << std::endl;

    delete point;
    // delete hackPoint; // Runtime error: pointer being freed was not allocated

    return 0;
}

