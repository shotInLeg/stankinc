# Компиляция из командной строки
Допустим мы имеем файл с программой main.cpp, в котором содержится код:
```cpp
#include <iostream>

int main() {
    std::cout << "Hello, World!!!" << std::endl;
    return 0;
}
``` 
Тогда для компиляции нам необходимо написать в командной строке (bash/sh/zsh etc.)

gcc / g++
```bash
g++ main.cpp -o my_program -std=c++11
```

clang / clang++
```bash
clang++ main.cpp -o my_program -std=c++11
```

После чего программу можно будет запустить так
```bash
./my_program
```

